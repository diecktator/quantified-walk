package tracks

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/types"
	geojson "github.com/paulmach/go.geojson"
	"github.com/twpayne/go-geom"
	"gitlab.com/diecktator/420m/utils"
	"io/ioutil"
	"net/http"
	"time"
)

type App struct {
	Db *pg.DB
}

func (a *App) CreateTrack(w http.ResponseWriter, r *http.Request) {
	rawBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}
	var input *geojson.FeatureCollection
	input, err = geojson.UnmarshalFeatureCollection(rawBody)
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}

	start, err := time.Parse(time.RFC3339, input.Features[0].Properties["timestamp"].(string))
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}
	end, err := time.Parse(time.RFC3339, input.Features[len(input.Features)-1].Properties["timestamp"].(string))
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}

	track := Tracks{
		Start:    start,
		End:      end,
		Duration: int64(end.Sub(start).Minutes()),
	}
	_, err = a.Db.Model(&track).Insert()
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}

	var coords []geom.Coord

	type properties struct {
		Timestamp   time.Time `json:"timestamp"`
		Rain        float64   `json:"rain"`
		Snow        float64   `json:"snow"`
		Clouds      int64     `json:"clouds"`
		Temperature float64   `json:"temperature"`
	}

	for _, feature := range input.Features {
		pts := feature.Geometry.Point
		if len(pts) < 3 {
			pts = append(pts, -9999)
		}

		var props properties
		propsRaw, err := json.Marshal(feature.Properties)
		if err != nil {
			utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
			return
		}
		err = json.Unmarshal(propsRaw, &props)
		if err != nil {
			utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
			return
		}
		coord := geom.NewPointFlat(geom.XY, feature.Geometry.Point)
		coords = append(coords, coord.Coords())

		wp := Waypoints{
			TrackID:     track.ID,
			Geom:        types.Safe(fmt.Sprintf("ST_GeomFromText('POINT(%v %v %v)')", pts[0], pts[1], pts[2])),
			Clouds:      props.Clouds,
			Rain:        props.Rain,
			Snow:        props.Snow,
			Temperature: props.Temperature,
			Timestamp:   props.Timestamp,
		}
		_, err = a.Db.Model(&wp).Insert()
		if err != nil {
			utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
			return
		}
	}

	line := geom.NewLineString(geom.XY)
	line, err = line.SetCoords(coords)
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}
	length := line.Length()
	track.Distance = length * 100
	fmt.Println(track.Distance)

	_, err = a.Db.Model(&track).WherePK().Update()
	if err != nil {
		utils.RespondWithErrorMessage(http.StatusInternalServerError, err, w)
		return
	}
}
