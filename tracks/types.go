package tracks

import (
	"github.com/go-pg/pg/v10/types"
	"time"
)

type Tracks struct {
	ID       int64
	Start    time.Time
	End      time.Time
	Distance float64
	Duration int64
}

type Waypoints struct {
	ID          int64
	Geom        types.Safe `pq:",type:geometry(POINTZ,4326)"`
	Timestamp   time.Time
	Rain        float64
	Snow        float64
	Temperature float64
	Clouds      int64
	TrackID     int64
	Track       *Tracks `pg:"rel:has-one"`
}
