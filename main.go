package main

import (
	"fmt"
	"github.com/Netflix/go-env"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/gorilla/mux"
	"gitlab.com/diecktator/420m/tracks"
	"log"
	"net/http"
	"os"
	"time"
)

type Environment struct {
	PostgresHost     string `env:"POSTGRES_HOST"`
	PostgresPort     int64  `env:"POSTGRES_PORT"`
	PostgresUser     string `env:"POSTGRES_USER"`
	PostgresPassword string `env:"POSTGRES_PASSWORD"`
	PostgresDb       string `env:"POSTGRES_DB"`
}

type App struct {
	Db     *pg.DB
	Tracks tracks.App
}

func main() {
	var environment Environment
	_, err := env.UnmarshalFromEnviron(&environment)
	if err != nil {
		panic(err)
	}

	db := pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%d", environment.PostgresHost, environment.PostgresPort),
		User:     environment.PostgresUser,
		Password: environment.PostgresPassword,
		Database: environment.PostgresDb,
	})
	defer db.Close()

	a := App{
		Db: db,
		Tracks: tracks.App{
			Db: db,
		},
	}
	a.initDb()

	r := mux.NewRouter()
	r.HandleFunc("/tracks/", a.Tracks.CreateTrack).Methods(http.MethodPost)
	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func (a *App) initDb() {
	err := a.executeSqlFile("./schemas/init.sql")
	if err != nil {
		panic(err)
	}

	err = a.createSchema()
	if err != nil {
		panic(err)
	}

	err = a.executeSqlFile("./schemas/add_geom_column_waypoints.sql")
	if err != nil {
		panic(err)
	}
}

func (a *App) createSchema() error {
	models := []interface{}{
		(*tracks.Tracks)(nil),
		(*tracks.Waypoints)(nil),
	}

	for _, model := range models {
		err := a.Db.Model(model).CreateTable(&orm.CreateTableOptions{
			IfNotExists:   true,
			FKConstraints: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *App) executeSqlFile(filename string) error {
	dat, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	_, err = a.Db.Exec(string(dat))
	if err != nil {
		return err
	}
	return nil
}
