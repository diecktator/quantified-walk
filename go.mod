module gitlab.com/diecktator/420m

go 1.18

require (
	github.com/Netflix/go-env v0.0.0-20220526054621-78278af1949d
	github.com/go-pg/pg/v10 v10.10.6
)

require (
	github.com/go-pg/zerochecker v0.2.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/paulmach/go.geojson v1.4.0 // indirect
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/twpayne/go-geom v1.4.3 // indirect
	github.com/vmihailenco/bufpool v0.1.11 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.4 // indirect
	github.com/vmihailenco/tagparser v0.1.2 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	mellium.im/sasl v0.2.1 // indirect
)
