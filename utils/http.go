package utils

import (
	"fmt"
	"net/http"
)

func RespondWithErrorMessage(httpCode int, err error, w http.ResponseWriter) {
	fmt.Println(err)
	w.WriteHeader(httpCode)
	fmt.Fprintf(w, "{\"error\": \"%v\"}", err)
}
